export interface ProductoI extends Document {
	_id?: string,
	name: string,
	description: string,
	image: string,
	price: Number,
	nutricional: string

