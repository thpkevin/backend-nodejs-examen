export interface NutricionalValueI extends Document {
	_id             ?: string,
	calories         ?: Number,
	protein          ?: Number,
	carbohydrates    ?: Number,
	sugars           ?: Number,
	grease           ?: Number,
	saturated_fat    ?: Number,
	trans_fat        ?: Number,
	trans_fsodiumat  ?: Number,
}

