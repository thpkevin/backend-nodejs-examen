import mongoose from 'mongoose';

const dbConnectionMongo = async () => {
	try {
		await mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser: true,useUnifiedTopology: true,useCreateIndex: true});
		console.log('Conexión establecida a la BD.');
	} catch (error) {
		console.error(error);
		throw new Error('Error Interno: Favor de comunicarse con el administrador del sistema');
	}
}

export default dbConnectionMongo;