import express from 'express';
import cors from 'cors';
import { config } from 'dotenv';
import dbConnectionMongo from './database/config';
import productRoutes from './routes/product.routes';

// configuracion inicial
config();
dbConnectionMongo();

const app = express();

app.use(cors());
app.use(express.json());

//rutas
app.use('/api/product', productRoutes );

app.listen(process.env.PORT, () => {console.log(`Servidor en el PUERTO: ${process.env.PORT}`);})



