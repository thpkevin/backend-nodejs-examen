import { Router } from 'express';
import { check } from 'express-validator';
import { create, getAll, getById, update } from '../controllers/product.controller';
import { fieldValid } from '../middlewares/ValidProduct.middleware';

const router = Router();

router.post('/', [
	check('name', 'El campo nombre es requerido').notEmpty(),
	check('image', 'El campo nombre es requerido').notEmpty(),
	check('price', 'El campo nombre es requerido').notEmpty(),
	check('price', 'El campo nombre es requerido').notEmpty(),
	check('description', 'El campo nombre es requerido').notEmpty(),
	check('nutricional', 'El campo nombre es requerido').notEmpty(),
	fieldValid
], create);

router.get('/', getAll);
router.get('/:id', getById);
router.patch('/', update)

export default router;

