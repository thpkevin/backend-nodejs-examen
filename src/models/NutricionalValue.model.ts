import { Schema, model } from 'mongoose';
import { NutricionalValueI } from '../domain/NutricionalValueI.model';

const NutricionalValue = new Schema({
    calories: {
		type: Number,
		required: false
	},
    protein: {
		type: Number,
		required: false
	},
    carbohydrates: {
		type: Number,
		required: false
	},
    sugars: {
		type: Number,
		required: false
	},
    grease: {
		type: Number,
		required: false
	},
    saturated_fat: {
		type: Number,
		required: false
	},
    trans_fat: {
		type: Number,
		required: false
	},
    sodium: {
		type: Number,
		required: false
	}
});


export default model<NutricionalValueI>('nutricional_value', NutricionalValue);