import { Schema, model } from 'mongoose';
import { ProductoI } from '../domain/ProductI.model';

const Product = new Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		requried: true
	},
	image: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	nutricional: {
		type: Schema.Types.ObjectId,
		ref: 'nutricional_value',
		required: true
	}
});

export default model<ProductoI>('product', Product);