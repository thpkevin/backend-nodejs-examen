import { Response, Request } from 'express';
import Product from '../models/Product.model';
import NutricionalValue from '../models/NutricionalValue.model';
import { networkError, serverError, networkSuccess } from '../middlewares/response.middleware';

const create = async (req: Request, res: Response) => {
	try {
		// producto del valor nutricional
		const { nutricional } = req.body
		const nutricional_value =  new NutricionalValue(nutricional);
		await nutricional_value.save();

		// creacion del producto
		req.body.nutricional = nutricional_value._id;
		const product =  new Product(req.body);
		await product.save();

		networkSuccess(res, product, 'Producto creado', 201);
	} catch (error) {
		serverError(res, error);
	}
}

const getAll = async (req: Request, res: Response) => {
	try {
		const Products = await Product.find();
		networkSuccess(res, Products, 'Lista de Productos');
	} catch (error) {
		serverError(res, error);
	}
}

const getById = async (req: Request, res: Response) => {
	try {
		const { id } = req.params;
		const product = await Product.findById(id).populate('nutricional');
		if(product){
			networkSuccess(res, product, 'Producto encontrado')
		}
		networkError(res, 'Producto no encontrado', 'Favor de volver a intentar.', 404);
	} catch (error) {
		serverError(res, error);
	}
}

const update = async (req: Request, res: Response) => {
	try {
		const { nutricional } = req.body;
		// actualizando el valor nutricional
		await NutricionalValue.updateOne({_id: nutricional._id}, {...nutricional});
		req.body.nutricional = nutricional._id;

		// actualizando el producto
		await Product.updateOne({_id: req.body._id}, {...req.body});
		networkSuccess(res, null, 'Producto actualizado.')
	} catch (error) {
		serverError(res, error);
	}
}

export { 
	create,
	getAll,
	getById,
	update
}