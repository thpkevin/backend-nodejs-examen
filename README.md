# EXAMEN - Backend NodeJS


# CREATE:
	URL: http://localhost:3000/api/product/60b27451d28faf716d9f4af9
	METHOD: POST
	BODY: 
	{
		"name":         "La Charly",
		"description":  "Hamburguesa a la parrilla, salsa tártara golf, papas al hilo, lomo ahumado de cerdo y lechuga Mayonesa x1, Ketchup x1, Ají Bembos x1",
		"image": "https://d31npzejelj8v1.cloudfront.net/media/catalog/product/b/e/bembos-delivery-la-charly.webp",
		"price": 9.90,
		"nutricional":{
			"calories"         : 10.3,
			"protein"          : 14,
			"carbohydrates"    : 304,
			"sugars"           : 2,
			"grease"           : 44,
			"saturated_fat"    : 55,
			"trans_fat"        : 28,
			"trans_fsodiumat"  : 49
		}
	}

# GET ALL:
	URL: http://localhost:3000/api/product
	METHOD: GET


# GET BY ID:
	URL: http://localhost:3000/api/product/60b27451d28faf716d9f4af9
	METHOD: GET


# UPDATE:
	URL: http://localhost:3000/api/product/60b27451d28faf716d9f4af9
	METHOD: PATCH
	BODY:
	{
		"_id": "60b27451d28faf716d9f4af9",
		"name": "La Charly",
		"description": "Hamburguesa a la parrilla, salsa tártara golf, papas al hilo, lomo ahumado de cerdo y lechuga Mayonesa x1, Ketchup x1, Ají Bembos x1",
		"image": "https://d31npzejelj8v1.cloudfront.net/media/catalog/product/b/e/bembos-delivery-la-charly.webp",
		"price": 20.9,
		"nutricional": {
			"_id": "60b27451d28faf716d9f4af8",
			"calories": 10.3,
			"protein": 14,
			"carbohydrates": 304,
			"sugars": 2,
			"grease": 44,
			"saturated_fat": 55,
			"trans_fat": 0
		}
	}


